using System;
using System.Collections.Generic;

[Serializable()]
public class Player {
        public string name { get; set; }
        public string id { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
    }
[Serializable()]
public class Scorejson {
        public int id { get; set; }
        public int score { get; set; }
        public string gametype { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
    }

[Serializable()]
public class Scores
{
    public List<Scorejson> hard { get; set; }
    public List<Scorejson> normal { get; set; }
    public int playerHard { get; set; }
    public int playerNormal { get; set; }
}