﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System;
using Newtonsoft.Json;

public class Settings : MonoBehaviour {

    public Button controlsButton;
    public Button modeButton;

    public Sprite enabledSprite;
    public Sprite disabledSprite;

    public InputField usernameInputField;

    public Text currentNameText;

    string username;

    int alternativeControls;
    int hardMode;

    void Start() {
        alternativeControls = PlayerPrefs.GetInt("alternativeControls");
        hardMode = PlayerPrefs.GetInt("hardmode");

        if (alternativeControls == 1) {
            controlsButton.image.sprite = enabledSprite;
        }
        else controlsButton.image.sprite = disabledSprite;

        if (hardMode == 1) {
            modeButton.image.sprite = enabledSprite;
        }
        else modeButton.image.sprite = disabledSprite;

        SetUsernameText();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) back();
    }

    public void back() {
        Application.LoadLevel("start01");
    }

    public void changeControls() {
        if (alternativeControls == 1) {
            controlsButton.image.sprite = disabledSprite;
            PlayerPrefs.SetInt("alternativeControls", 0);
            alternativeControls = 0;
        }
        else {
            controlsButton.image.sprite = enabledSprite;
            PlayerPrefs.SetInt("alternativeControls", 1);
            alternativeControls = 1;
        }
    }

    public void changeMode() {
        if (hardMode == 1) {
            modeButton.image.sprite = disabledSprite;
            PlayerPrefs.SetInt("hardmode", 0);
            hardMode = 0;
        }
        else {
            modeButton.image.sprite = enabledSprite;
            PlayerPrefs.SetInt("hardmode", 1);
            hardMode = 1;
        }
    }

    public void saveUsername() {
        username = usernameInputField.text;

        if (username == "_noads_") {
            PlayerPrefs.SetInt("ads", 1);
        }
        else if (username == "_ads_") {
            PlayerPrefs.SetInt("ads", 0);
        }
        else {
            try {
                if (string.IsNullOrEmpty(PlayerPrefs.GetString("id"))) register();
                else ChangeName();

                PlayerPrefs.SetString("username", username);
                SetUsernameText();
            }
            catch {
                currentNameText.text = "no internet connection";
            }
        }
    }

    void register() {
        using (var client = new WebClient()) {
            var values = new NameValueCollection();
            values["name"] = username;

            var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/RegisterPlayer", values);

            var responseString = Encoding.Default.GetString(response);
            var temp = JsonConvert.DeserializeObject<id>(responseString);
            PlayerPrefs.SetString("id", temp.Id);
        }

        using (var client = new WebClient()) {
            var values = new NameValueCollection();
            values["id"] = PlayerPrefs.GetString("id");
            values["gametype"] = "hard";
            values["score"] = PlayerPrefs.GetFloat("hardScore").ToString();

            var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/SaveScore", values);
        }
        using (var client = new WebClient()) {
            var values = new NameValueCollection();
            values["id"] = PlayerPrefs.GetString("id");
            values["gametype"] = "normal";
            values["score"] = PlayerPrefs.GetFloat("normalScore").ToString();

            var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/SaveScore", values);
        }
    }

    void ChangeName() {
        using (var client = new WebClient()) {
            var values = new NameValueCollection();
            values["name"] = username;
            values["id"] = PlayerPrefs.GetString("id");

            var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/ChangeName", values);
        }
    }   

    void SetUsernameText() {
        username = PlayerPrefs.GetString("username");

        if (username == "") currentNameText.text = "Username: no username";
        else currentNameText.text = "Username: " + username;
    }
}


public class id {
    public string Result { get; set; }
    public string Id { get; set; }
}
