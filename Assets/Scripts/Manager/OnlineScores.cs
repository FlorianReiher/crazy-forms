﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;

public class OnlineScores : MonoBehaviour {

    bool hard = false;

    Scores scores;

    public Button modeButton;

    public Text onlineScoresText;
    public Text placeText;

    public Sprite enabledSprite;
    public Sprite disabledSprite;

	// Use this for initialization
	void Start () {
        try {
            GetScores();
        }
        catch {
            onlineScoresText.text = "no internet connection";
        }
	}

    public void ChangeMode() {
        hard = !hard;
        if (hard) {
            modeButton.image.sprite = enabledSprite;
        }
        else {
            modeButton.image.sprite = disabledSprite;
        }

        onlineScoresText.text = "";

        Fill();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) back();
    }

    void GetScores() {
        using (var client = new WebClient()) {
            var values = new NameValueCollection();
            values["id"] = PlayerPrefs.GetString("id");

            var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/GetScores/50", values);

            var responseString = Encoding.Default.GetString(response);
            scores = JsonConvert.DeserializeObject<Scores>(responseString);
            
            Fill();
        }
    }

    void Fill() {
        if (hard) {
            Fill1(scores.hard, scores.playerHard);
        }
        else {
            Fill1(scores.normal, scores.playerNormal);
        }
    }

    void Fill1(List<Scorejson> list, int place) {
        int i = 1;
        foreach (var item in list) {
            onlineScoresText.text += string.Format(" {0}. {1}: {2} \n", i, item.Player.name, item.score);
            i++;
        }

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("id"))) {
            placeText.text = "no id";
        }
        else {
            placeText.text = "Your Rank: " + place.ToString();
        }
    }
	
    public void back() {
        Application.LoadLevel("start01");
    }
}
