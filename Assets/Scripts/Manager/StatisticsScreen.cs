﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatisticsScreen : MonoBehaviour {

    public Text normalStatsText;
    public Text hardStatsText;

    string normalGamesPlayed;
    string normalAverage;
    string normalHighscore;

    string hardGamesPlayed;
    string hardAverage;
    string hardHighscore;

	// Use this for initialization
	void Start () {
       // PlayerPrefs.SetInt("gamesPlayedNormal", 0);
        //PlayerPrefs.SetFloat("averageScoreNormal", 0f);

        normalGamesPlayed = PlayerPrefs.GetInt("gamesPlayedNormal").ToString();
        normalAverage = Mathf.Round(PlayerPrefs.GetFloat("averageScoreNormal")).ToString();
        normalHighscore = PlayerPrefs.GetFloat("normalScore").ToString();
        normalStatsText.text = "Highscore: " + normalHighscore + "\nGames played: " + normalGamesPlayed + "\nAverage Score: " + normalAverage;

        hardGamesPlayed = PlayerPrefs.GetInt("gamesPlayedHard").ToString();
        hardAverage = Mathf.Round(PlayerPrefs.GetFloat("averageScoreHard")).ToString();
        hardHighscore = PlayerPrefs.GetFloat("hardScore").ToString();
        hardStatsText.text = "Highscore: " + hardHighscore + "\nGames played: " + hardGamesPlayed + "\nAverage Score: " + hardAverage;
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) back();
    }

    public void back() {
        Application.LoadLevel("start01");
    }
}
