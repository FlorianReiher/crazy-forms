﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Button againButton;
    public Button settingsButton;
    public Button weiterButton;
    public Button homeButton;

    public Text highscoreText;
    public Text modeText;

    int hardmode;

    // Use this for initialization
    void Start () {
        againButton.gameObject.SetActive(false);
        settingsButton.gameObject.SetActive(false);
        weiterButton.gameObject.SetActive(false);
        homeButton.gameObject.SetActive(false);

        highscoreText.gameObject.SetActive(false);
        modeText.gameObject.SetActive(false);

        hardmode = PlayerPrefs.GetInt("hardmode");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (movement.dead) {
            againButton.gameObject.SetActive(true);
            settingsButton.gameObject.SetActive(true);
            homeButton.gameObject.SetActive(true);

            highscoreText.gameObject.SetActive(true);
            modeText.gameObject.SetActive(true);
        }

        if (hardmode == 1) {
            modeText.text = "hard mode";
            highscoreText.text = "Highscore:\n" + PlayerPrefs.GetFloat("hardScore");
        }
        else {
            modeText.text = "normal mode";
            highscoreText.text = "Highscore:\n" + PlayerPrefs.GetFloat("normalScore");
        }
    }

    public void reset() {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void options() {
        Application.LoadLevel("options01");
    }
    public void setPause() {
        if (!movement.dead) {
            Time.timeScale = 0.0f;
            weiterButton.gameObject.SetActive(true);
            homeButton.gameObject.SetActive(true);
            movement.pause = true;
        }
    }
    public void weiter() {
        Time.timeScale = 1.0f;
        weiterButton.gameObject.SetActive(false);
        homeButton.gameObject.SetActive(false);
        movement.pause = false;
    }
    public void home() {
        Time.timeScale = 1f;
        movement.pause = false;
        Application.LoadLevel("start01");
    }
}
