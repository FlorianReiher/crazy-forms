﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.Collections.Specialized;

public class StartScreen : MonoBehaviour {

    float hardScore;
    float normalScore;
    float registeredTimer;

    public Text scoreText;
    public Text welcomeText;
    public Text registeredText;

    string username;

    void Start() {
        hardScore = PlayerPrefs.GetFloat("hardScore");
        normalScore = PlayerPrefs.GetFloat("normalScore");
        username = PlayerPrefs.GetString("username");

        welcomeText.text = "Welcome " + username;
        scoreText.text = "normal: " + normalScore + "\nhard:  " + hardScore;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

        if (registeredTimer > 0) {
            registeredText.gameObject.SetActive(true);
            registeredTimer -= Time.deltaTime;
        }
        else {
            registeredText.gameObject.SetActive(false);
        }
    }

    public void play() {
        Application.LoadLevel("game01");
    }

    public void settings() {
        Application.LoadLevel("options01");
    }

    public void toOnline() {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("username"))) {
            using (var client = new WebClient()) {
                var values = new NameValueCollection();
                values["id"] = PlayerPrefs.GetString("id");
                values["gametype"] = "hard";
                values["score"] = PlayerPrefs.GetFloat("hardScore").ToString();

                var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/SaveScore", values);
            }
            using (var client = new WebClient()) {
                var values = new NameValueCollection();
                values["id"] = PlayerPrefs.GetString("id");
                values["gametype"] = "normal";
                values["score"] = PlayerPrefs.GetFloat("normalScore").ToString();

                var response = client.UploadValues("http://crazyforms.michaelbeier.biz/api/SaveScore", values);
            }

            Application.LoadLevel("online01");
        }
        else {
            registeredTimer = 1f;
        }
    }

    public void toStatistics() {
        Application.LoadLevel("statistics01");
    }
}
