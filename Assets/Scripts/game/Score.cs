﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.Collections.Specialized;
using System.Text;

public class Score : MonoBehaviour {

	public static float score;

    float averageScoreNormal;
    float averageScoreHard;
    int gamesPlayedNormal;
    int gamesPlayedHard;

    int hardMode;

    bool stillDead = false;

    public Text scoreText;

    void Start(){
		score = 0f;
        hardMode = PlayerPrefs.GetInt("hardmode");

        averageScoreNormal = PlayerPrefs.GetFloat("averageScoreNormal");
        averageScoreHard = PlayerPrefs.GetFloat("averageScoreHard");
        gamesPlayedNormal = PlayerPrefs.GetInt("gamesPlayedNormal");
        gamesPlayedHard = PlayerPrefs.GetInt("gamesPlayedHard");
	}

	void FixedUpdate(){
        if (movement.dead == true) {
            if (hardMode == 1 & score > PlayerPrefs.GetFloat("hardScore")) {
                PlayerPrefs.SetFloat("hardScore", score);
            }
            else if (hardMode == 0 & score > PlayerPrefs.GetFloat("normalScore")) {
                PlayerPrefs.SetFloat("normalScore", score);
            }

            if (!stillDead) average();
            stillDead = true;
        }
        else stillDead = false;

	}

    void average() {
        if (hardMode == 1) {
            averageScoreHard = ((gamesPlayedHard * averageScoreHard) + score) / (gamesPlayedHard + 1);
            PlayerPrefs.SetFloat("averageScoreHard", averageScoreHard);
            gamesPlayedHard++;
            PlayerPrefs.SetInt("gamesPlayedHard", gamesPlayedHard);
        }
        else {
            averageScoreNormal = ((gamesPlayedNormal * averageScoreNormal) + score) / (gamesPlayedNormal + 1);
            PlayerPrefs.SetFloat("averageScoreNormal", averageScoreNormal);
            gamesPlayedNormal++;
            PlayerPrefs.SetInt("gamesPlayedNormal", gamesPlayedNormal);
        }
    }

	void OnTriggerEnter(Collider collider){
		if (collider.name == "Walls" & movement.speed.y > 0) {
			score += 0.5f;
			scoreText.text = score.ToString ();
		}
	}
}

