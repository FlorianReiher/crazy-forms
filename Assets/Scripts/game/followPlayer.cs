﻿using UnityEngine;
using System.Collections;

public class followPlayer : MonoBehaviour {

	GameObject player;

	float offsetY;

	Vector3 cameraPos;

	void Start () {
		player = GameObject.FindWithTag ("Player");

		offsetY = transform.position.y - player.transform.position.y;
	}
	

	void FixedUpdate () {
		if (movement.speed.y > 0f & (transform.position.y - player.transform.position.y) < offsetY) {
			cameraPos = transform.position;
			cameraPos.y = player.transform.position.y + offsetY;

			transform.position = cameraPos;
		}
	}
}
