﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	float leftborder = -2.5f;
	float rightborder = 2.5f;
    float moveSpeed = 1.5f;
    float difficultyScore = 5;

	bool right = true;

    int hardmode;

	Vector3 pos;

    Renderer rend;

    public Material[] materials;

	// Use this for initialization
	void Start () {
		pos = transform.position;
        hardmode = PlayerPrefs.GetInt("hardmode");

        rend = GetComponent<Renderer>();
        rend.enabled = true;

        int d = Random.Range(0, 5);
        rend.sharedMaterial = materials[d];
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (!movement.pause) {
            if (transform.position.x < rightborder & right) {
                pos.x += moveSpeed * Time.deltaTime;
                transform.position = pos;
            }
            else if (transform.position.x > rightborder & right) {
                right = false;
            }
            else if (transform.position.x > leftborder & !right) {
                pos.x -= moveSpeed * Time.deltaTime;
                transform.position = pos;
            }
            else if (transform.position.x < leftborder & !right) {
                right = true;
            }
        }

        if (hardmode == 1) {
            if (Score.score >= difficultyScore & moveSpeed <= 5f) {
                moveSpeed += 0.2f;
                difficultyScore += 5f;
            }
        }
	}

	void OnTriggerEnter(Collider collider){
		pos.y += 18;
		pos.x = Random.Range (leftborder, rightborder);
		transform.position = pos;

		float c = Random.Range (0f, 2f);
		if (c <= 1) {
			right = true;
		} 
		else {
			right = false;
		}

        int d = Random.Range(0, 5);
        rend.sharedMaterial = materials[d];
	}
}
