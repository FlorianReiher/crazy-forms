﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class movement : MonoBehaviour {

    float upSpeed = 6f;
    float sidespeed = 1f;
    float jumpTimer = 0f;

	static public Vector3 speed;

	bool up = false;
    bool upRight = false;
    bool upLeft = false;
	static public bool dead = false;
    public static bool pause = false;

    int alternativeControls = 0;

    Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		rb.useGravity = false;

		dead = false;
        alternativeControls = PlayerPrefs.GetInt("alternativeControls");
	}

    // Update is called once per frame
    void Update() {
        if (dead | pause) {
            return;
        }

        if (Input.GetMouseButtonDown(0) & Input.mousePosition.y < Screen.height * (9f/10f)) {
            up = true;

            if (alternativeControls == 1) {
                if (Input.mousePosition.x < (Screen.width / 2f)) {
                    upLeft = true;
                }
                else upRight = true;
            }
        }

        speed = rb.velocity;

        if (jumpTimer > 0) {
            rb.useGravity = false;
            jumpTimer -= Time.deltaTime;
        }
        else if (rb.velocity != Vector3.zero) {
            rb.useGravity = true;
        }

        if (up) {
            rb.useGravity = true;

            Vector3 tSpeed = rb.velocity;
            tSpeed.y = upSpeed;
            rb.velocity = tSpeed;

            up = false;
            jumpTimer = 0.1f;
        }

        if (upLeft) {
            Vector3 tSpeed = rb.velocity;
            tSpeed.x = -sidespeed;
            rb.velocity = tSpeed;

            upLeft = false;
        }
        else if (upRight) {
            Vector3 tSpeed = rb.velocity;
            tSpeed.x = sidespeed;
            rb.velocity = tSpeed;

            upRight = false;
        }
    }

	void OnTriggerEnter(Collider collider){
		if (collider.name == "Enemy" | collider.name == "Wall" | collider.name == "Main Camera") {
			dead = true;
			rb.AddTorque (3, 3, 3);
		}
	}
}
