﻿using UnityEngine;
using System.Collections;

public class WallsMovement : MonoBehaviour {

    float leftborder = 7f;
	float rightborder = 10f;
	float moveSpeed = 1f;
    float difficultyScore = 5f;

	bool right = true;

    int hardmode;

	Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
        hardmode = PlayerPrefs.GetInt("hardmode");
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (!movement.pause) {
            if (transform.position.x < rightborder & right) {
                pos.x += moveSpeed * Time.deltaTime;
                transform.position = pos;
            }
            else if (transform.position.x > rightborder & right) {
                right = false;
            }
            else if (transform.position.x > leftborder & !right) {
                pos.x -= moveSpeed * Time.deltaTime;
                transform.position = pos;
            }
            else if (transform.position.x < leftborder & !right) {
                right = true;
            }
        }

        if (hardmode == 1) {
            if (Score.score >= difficultyScore & moveSpeed <= 5f) {
                moveSpeed += 0.1f;
                difficultyScore += 5f;
            }
        }
	}

	void OnTriggerEnter(Collider collider){
		if (collider.name != "Player") {
			pos.y += 18;
            pos.x = Random.Range(leftborder, rightborder);
            transform.position = pos;

            float c = Random.Range(0f, 2f);
            if (c <= 1)
            {
                right = true;
            }
            else {
                right = false;
            }
        }
	}
}
