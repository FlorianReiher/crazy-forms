﻿using UnityEngine;
using System.Collections;

public class leftWallDifficulty : MonoBehaviour {

    Vector3 pos;

    float difficultyScore = 10f;

    int hardMode;

	// Use this for initialization
	void Start () {
        hardMode = PlayerPrefs.GetInt("hardmode");
	}
	
	// Update is called once per frame
	void Update () {
        pos = transform.position;

        if (hardMode == 1) {
            if (Score.score >= difficultyScore & difficultyScore < 90f) {
                pos.x += 0.1f;
                transform.position = pos;
                difficultyScore += 10;
            }
        }
	}
}
